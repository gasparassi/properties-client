import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: () => import('@/views/dashboard/Index'),
			children: [
				// Dashboard
				{
					name: 'dashboard',
					path: '',
					component: () => import('@/views/dashboard/Dashboard')
				},
				{
					name: 'properties',
					path: 'pages/property/index',
					component: () => import('@/views/pages/property/Property'),
				},
				{
					name: 'property-form',
					path: 'pages/property/form',
					component: () => import('@/views/pages/property/PropertyForm'),
				},
				{
					name: 'lease-agreements',
					path: 'pages/contract/index',
					component: () => import('@/views/pages/contract/LeaseAgreements'),
				},
				{
					name: 'lease-agreements-form',
					path: 'pages/contract/form',
					component: () => import('@/views/pages/contract/LeaseAgreementForm'),
				},
			]
		}
	]
});

export default router;
