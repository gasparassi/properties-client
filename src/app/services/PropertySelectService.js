import Rest from './Rest'

/**
 * @typedef {PropertySelectService}
 */
export default class PropertySelectService extends Rest {
  /**
   * @type {String}
   */
  static resource = '/properties/select'
}
