import Rest from './Rest'

/**
 * @typedef {PropertyService}
 */
export default class PropertyService extends Rest {
  /**
   * @type {String}
   */
  static resource = '/properties'

  rules = {
    required: (value) => !!value || "Este campo é obrigatório",
    emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
  }

}
