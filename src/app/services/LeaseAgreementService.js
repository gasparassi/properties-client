import Rest from './Rest'

/**
 * @typedef {LeaseAgreementService}
 */
export default class LeaseAgreementService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/lease-agreements'

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
    emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
  }

  maskCPF_CNPJ(value){
    if (!! value) {
      return value.length === 14 ? '###.###.###-##' : '##.###.###/####-##';
    } else{
      return '###.###.###-##';
    }
  }

}
