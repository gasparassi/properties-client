/**
 * Property Model
 */

export default class PropertyModel {

  id = null;
  owner_email = "";
  street = "";
  number = 0;
  complement = "";
  neighborhood = "";
  city = "";
  state = "";
}
