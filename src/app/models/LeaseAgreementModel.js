/**
 * LeaseAgreement Model
 */

export default class LeaseAgreementModel {

  id = null;
  property_id = null;
  person_or_entity = true;
  cpf_cnpj = "";
  contractor_email = "";
  contractor_name = "";
}
