import LeaseAgreementService from '../services/LeaseAgreementService';
import LeaseAgreementModel from '../models/LeaseAgreementModel';

/**
 * @typedef {LeaseAgreementController}
 */
export default class LeaseAgreementController {

    /**
    * @type {null}
     */

    service = null;

    constructor() {
        this.service = LeaseAgreementService.build();
    }

    leaseAgreementModel() {
        return new LeaseAgreementModel();
    }

    _removeSpecialsCaracters(field) {
        field = field.replaceAll('.', '');
        field = field.replace('-', '');
        field = field.replace('/', '');

        return field;
    }

    async register(leaseAgreement) {
        leaseAgreement.cpf_cnpj = this._removeSpecialsCaracters(leaseAgreement.cpf_cnpj);
        return await this.service.create(leaseAgreement).then(
            response => {
                return response;
            }
        );
    }

    async getAll() {
        return await this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

    async getOne(id) {
        return await this.service.read(id).then(
            response => {
                return response;
            }
        );
    }

    async delete(leaseAgreement) {
        return await this.service.destroy(leaseAgreement).then(
            response => {
                return response;
            }
        );
    }

    async update(leaseAgreement) {
        leaseAgreement.cpf_cnpj = this._removeSpecialsCaracters(leaseAgreement.cpf_cnpj);
        leaseAgreement.property_id = leaseAgreement.property.id;
        return await this.service.update(leaseAgreement).then(
            response => {
                return response;
            }
        );
    }

}
