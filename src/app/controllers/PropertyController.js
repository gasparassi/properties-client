import PropertyService from '../services/PropertyService';
import PropertySelectService from '../services/PropertySelectService';
import PropertyModel from '../models/PropertyModel';

/**
 * @typedef {PropertyController}
 */
export default class PropertyController {

    /**
    * @type {null}
     */

    service = null;

    constructor() {
        this.service = PropertyService.build();
    }

    propertyModel() {
        return new PropertyModel();
    }

    _mountArrayPropertiesForSelect(data) {

        let fullAddressesForSelect = [];

        if (data.length > 0) {
            data.forEach((element) => {
                fullAddressesForSelect.push({
                    property_id: element.id,
                    fullAddress: `${element.street}, ${element.number}, ${element.complement}, ${element.neighborhood}`
                })
            });
        }

        return fullAddressesForSelect;
    }

    async getAllPropertiesForSelect() {
        return await PropertySelectService.build().search(null).then(
            response => {
                return this._mountArrayPropertiesForSelect(response.rows.data);
            }
        );
    }

    async register(property) {
        return await this.service.create(property).then(
            response => {
                return response;
            }
        );
    }

    async getAll() {
        return await this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

    async getOne(id) {
        return await this.service.read(id).then(
            response => {
                return response;
            }
        );
    }

    async delete(property) {
        return await this.service.destroy(property).then(
            response => {
                return response;
            }
        );
    }

    async update(property) {
        return await this.service.update(property).then(
            response => {
                return response;
            }
        );
    }

}
